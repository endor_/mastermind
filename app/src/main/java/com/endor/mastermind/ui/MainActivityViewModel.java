package com.endor.mastermind.ui;

import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.endor.mastermind.model.ButtonKey;
import com.endor.mastermind.model.Game;
import com.endor.mastermind.model.GameState;

public class MainActivityViewModel extends ViewModel {

    final private Game game;

    private MutableLiveData<GameState> gameState;

    public LiveData<GameState> getGameState() {
        return gameState;
    }

    public MainActivityViewModel(Game game) {
        this.game = game;
        this.gameState = new MutableLiveData<>(new GameState(null, game.getLEDColors(), false));
    }

    public void resetGame() {
        game.reset();
        gameState.setValue(new GameState(null, game.getLEDColors(), false));
    }

    public void onButtonClick(View view) {
        ButtonKey buttonKey = (ButtonKey) view.getTag();
        game.onButtonPressed(buttonKey);
        gameState.setValue(
                new GameState(game.getGuessString(), game.getLEDColors(), game.answerFound())
        );
    }
}