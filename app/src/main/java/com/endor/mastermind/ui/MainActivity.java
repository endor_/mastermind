package com.endor.mastermind.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.endor.mastermind.R;
import com.endor.mastermind.databinding.ActivityMainBinding;
import com.endor.mastermind.model.Game;
import com.endor.mastermind.model.GameState;

public class MainActivity extends AppCompatActivity {

    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider(MainActivity.this, viewModelFactory)
                .get(MainActivityViewModel.class);

        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);

        binding.setLifecycleOwner(MainActivity.this);
        binding.setViewModel(viewModel);

        viewModel.getGameState().observe(MainActivity.this, new Observer<GameState>() {
            @Override
            public void onChanged(GameState gameState) {
                binding.tvGuess.setText(gameState.getGuess());
                updateLEDColors(gameState.getLEDColors());

                if (gameState.isEnded()) {
                    showResetDialog();
                }
            }
        });
    }

    private ViewModelProvider.Factory viewModelFactory = new ViewModelProvider.Factory() {
        @SuppressWarnings("unchecked")
        @Override
        @NonNull
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new MainActivityViewModel(new Game(MainApplication.NUMBER_OF_LED));
        }
    };

    private void updateLEDColors(int[] colors) {
        switch (colors.length) {
            case 3:
                binding.iv3.setImageResource(colors[2]);
            case 2:
                binding.iv2.setImageResource(colors[1]);
            case 1:
                binding.iv1.setImageResource(colors[0]);
                break;
        }
    }

    private void showResetDialog() {
        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setMessage(R.string.game_end_message)
                .setPositiveButton(R.string.game_end_label_positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewModel.resetGame();
                    }
                })
                .setCancelable(false)
                .create();

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}