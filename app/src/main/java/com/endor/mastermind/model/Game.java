package com.endor.mastermind.model;

import com.endor.mastermind.R;
import com.endor.mastermind.ui.MainApplication;

import java.util.Arrays;
import java.util.Random;

/**
 * Holds state and transformation functions for a Game session
 */
public class Game {

    final private ButtonKey[] answer;

    private ButtonKey[] guess;

    private int[] LEDColors;

    private StringBuilder guessStringBuilder = new StringBuilder();

    private Random random = new Random();

    public Game(int lengthOfAnswer) {
        answer = new ButtonKey[lengthOfAnswer];
        generateAnswer(answer);
        guess = new ButtonKey[lengthOfAnswer];
        LEDColors = new int[lengthOfAnswer];
        Arrays.fill(LEDColors, R.drawable.circle_off);
    }

    private void generateAnswer(ButtonKey[] answer) {
        for (int i = 0; i < answer.length; i++) {
            answer[i] = ButtonKey.values()[random.nextInt(answer.length)];
        }
    }

    public Game onButtonPressed(ButtonKey key) {
        updateGuess(key);
        updateGuessString(key);
        updateLEDColors();
        return this;
    }

    public void reset() {
        generateAnswer(answer);
        Arrays.fill(guess, null);
        Arrays.fill(LEDColors, R.drawable.circle_off);
        clearGuessString();
    }

    public int[] getLEDColors() {
        return LEDColors;
    }

    public String getGuessString() {
        return guessStringBuilder.toString();
    }

    public boolean answerFound() {
        return Arrays.equals(answer, guess);
    }

    private void clearGuessString() {
        guessStringBuilder.delete(0, guessStringBuilder.length());
    }

    private void updateGuess(ButtonKey key) {
        int lastPosition = guess.length - 1;
        if (lastPosition >= 0) {
            System.arraycopy(guess, 1, guess, 0, lastPosition);
            guess[lastPosition] = key;
        }
    }

    private void updateGuessString(ButtonKey key) {
        if (guessStringBuilder.length() == MainApplication.NUMBER_OF_LED) {
            guessStringBuilder.deleteCharAt(0);
        }

        guessStringBuilder.append(key.name());
    }

    private void updateLEDColors() {
        for (int i = 0; i < guess.length; i++) {
            if (guess[i] == null) {
                LEDColors[i] = R.drawable.circle_off;
            } else if (guess[i] == answer[i]) {
                // position and value match
                LEDColors[i] = R.drawable.circle_green;
            } else {
                // search if input exists in answer at all
                LEDColors[i] = R.drawable.circle_red;
                for (ButtonKey key : answer) {
                    if (guess[i] == key) {
                        LEDColors[i] = R.drawable.circle_orange;
                        break;
                    }
                }
            }
        }
    }
}