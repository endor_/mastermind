package com.endor.mastermind.model;

public class GameState {

    final String guess;
    final private int[] LEDColors;
    final boolean isEnded;

    public GameState(String guess, int[] LEDColors, boolean isEnded) {
        this.guess = guess;
        this.LEDColors = LEDColors;
        this.isEnded = isEnded;
    }

    public String getGuess() {
        return guess;
    }

    public int[] getLEDColors() {
        return LEDColors;
    }

    public boolean isEnded() {
        return isEnded;
    }
}