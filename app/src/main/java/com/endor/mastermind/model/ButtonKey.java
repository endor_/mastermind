package com.endor.mastermind.model;

/**
 * Enum class for keys that can be in the answer
 */
public enum ButtonKey {
    A,
    B,
    C
}