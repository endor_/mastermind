package com.endor.mastermind.model;

import com.endor.mastermind.R;
import com.endor.mastermind.ui.MainApplication;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class GameTest {

    Game game;

    @Test
    public void testOnButtonPressed() {
        game = new Game(MainApplication.NUMBER_OF_LED);

        assertEquals("", game.getGuessString());

        Game mutatedGame = game.onButtonPressed(ButtonKey.A);

        assertNotNull(mutatedGame);
        assertEquals("A", game.getGuessString());

        for (int i = MainApplication.NUMBER_OF_LED - 2; i >= 0; i--) {
            Assert.assertEquals(R.drawable.circle_off, game.getLEDColors()[i]);
        }

        assertNotEquals(R.drawable.circle_off, game.getLEDColors()[MainApplication.NUMBER_OF_LED - 1]);
    }

    @Test
    public void testReset() {
        game = new Game(MainApplication.NUMBER_OF_LED);
        game.onButtonPressed(ButtonKey.A);

        assertEquals("A", game.getGuessString());

        game.reset();
        assertEquals("", game.getGuessString());

        for (int color : game.getLEDColors()) {
            assertEquals(R.drawable.circle_off, color);
        }
    }
}