package com.endor.mastermind.ui;

import android.view.View;

import com.endor.mastermind.model.ButtonKey;
import com.endor.mastermind.model.Game;
import com.endor.mastermind.model.GameState;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class MainActivityViewModelTest {

    @Mock
    Game game;

    MainActivityViewModel viewModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        viewModel = new MainActivityViewModel(game);
    }

    @Test
    public void testInitialization() {
        assertNotNull(viewModel.getGameState());
    }

    @Test
    public void testResetGame() {
        viewModel.resetGame();
        GameState state = viewModel.getGameState().getValue();

        assertNotNull(state);
        assertNull(state.getGuess());
        assertFalse(state.isEnded());
    }

    @Test
    public void testOnButtonClick() {
        View view = mock(View.class);
        when(view.getTag()).thenReturn(ButtonKey.A);
        when(game.getGuessString()).thenReturn("A");

        viewModel.onButtonClick(view);
        GameState state = viewModel.getGameState().getValue();

        assertNotNull(state);
        assertEquals(ButtonKey.A.name(), state.getGuess());
        assertFalse(state.isEnded());

        // -----

        when(view.getTag()).thenReturn(ButtonKey.B);
        when(game.getGuessString()).thenReturn("AB");

        viewModel.onButtonClick(view);
        state = viewModel.getGameState().getValue();

        assertNotNull(state);
        assertEquals("AB", state.getGuess());
        assertFalse(state.isEnded());

        // -----

        when(view.getTag()).thenReturn(ButtonKey.C);
        when(game.getGuessString()).thenReturn("ABC");

        viewModel.onButtonClick(view);
        state = viewModel.getGameState().getValue();

        assertNotNull(state);
        assertEquals("ABC", state.getGuess());
        assertFalse(state.isEnded());

        // test answer found

        when(view.getTag()).thenReturn(ButtonKey.A);
        when(game.getGuessString()).thenReturn("BCA");
        when(game.answerFound()).thenReturn(true);

        viewModel.onButtonClick(view);
        state = viewModel.getGameState().getValue();

        assertNotNull(state);
        assertEquals("BCA", state.getGuess());
        assertTrue(state.isEnded());
    }
}