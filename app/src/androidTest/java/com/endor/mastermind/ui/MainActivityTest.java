package com.endor.mastermind.ui;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.endor.mastermind.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testButtonPress() {
        onView(withId(R.id.btn_a)).perform(click());
        onView(withId(R.id.tv_guess)).check(matches(withText("A")));

        onView(withId(R.id.btn_b)).perform(click());
        onView(withId(R.id.tv_guess)).check(matches(withText("AB")));

        onView(withId(R.id.btn_c)).perform(click());
        onView(withId(R.id.tv_guess)).check(matches(withText("ABC")));

        onView(withId(R.id.btn_b)).perform(click());
        onView(withId(R.id.tv_guess)).check(matches(withText("BCB")));
    }
}
